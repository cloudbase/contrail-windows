#include <iomanip>
#include <sstream>
#include <stdarg.h>
#include <sys/resource.h>

char *strptime(const char *buf, const char *fmt, struct tm *tm) {
    std::istringstream str(buf);
    str.imbue(std::locale(setlocale(LC_ALL, nullptr)));
    str >> std::get_time(tm, fmt);
    if (str.fail()) {
        return 0;
    }
    return const_cast<char *>(buf + str.tellg());
}

#if (_MSC_VER < 1900)
int snprintf(char *str, size_t size, const char *format, ...) {
    va_list args;
    va_start(args, format);

    if (str && size > 0) {
        const int ret = vsnprintf_s(str, size, size - 1, format, args);
        if (ret >= 0) {
            va_end(args);
            return ret;
        }

        (void) vsnprintf_s(str, size, _TRUNCATE, format, args);
        const int count = _vscprintf(format, args);
        va_end(args);
        return count;
    }

    const int ret = _vscprintf(format, args);
    va_end(args);
    return ret;
}
#endif

int ffs(int val) {
    for (int i = 1, mask = 1; i <= 32; ++i, mask = mask << 1) {
        if (val & mask) {
            return i;
        }
    }
    return 0;
}

static struct rlimit limits = { UINT_MAX, UINT_MAX };

int getrlimit(int resource, struct rlimit *rlimits) {
    rlimits->rlim_cur = limits.rlim_cur;
    rlimits->rlim_max = limits.rlim_max;
    return 0;
}

int setrlimit(int resource, const struct rlimit *rlimits) {
    limits.rlim_cur = rlimits->rlim_cur;
    limits.rlim_max = rlimits->rlim_max;
    return 0;
}
