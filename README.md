For windows builds, a list of repositories need to be cloned and the resulting
directory hierarchy needs to respect a specific structure.

The best way to describe this is to list the git commands:

```
git clone https://bitbucket.org/cloudbase/contrail-controller.git  controller
git clone https://bitbucket.org/cloudbase/contrail-vrouter.git     vrouter
git clone https://bitbucket.org/cloudbase/contrail-third-party.git third_party

git clone https://bitbucket.org/cloudbase/contrail-build.git       tools/build
git clone https://bitbucket.org/cloudbase/contrail-sandesh.git     tools/sandesh
git clone https://bitbucket.org/cloudbase/contrail-generateDS.git  tools/generateds
git clone https://bitbucket.org/cloudbase/contrail-windows.git     tools/windows

git clone https://github.com/Juniper/contrail-neutron-plugin.git   openstack/neutron_plugin
git clone https://github.com/Juniper/contrail-nova-vif-driver.git  openstack/nova_contrail_vif

```

Build tools:

* Visual Studio 2013 or Visual Studio 2015 and Windows 10 SDK  
  https://www.visualstudio.com/en-us/downloads/download-visual-studio-vs.aspx
* git for windows (and its tools from usr/bin)  
  https://git-scm.com/download/win
* python 2 and libxml  
  https://www.python.org/downloads/windows  
  http://www.lfd.uci.edu/~gohlke/pythonlibs/#lxml
* scons and pywin32  
  https://sourceforge.net/projects/scons/files/scons  
  http://pywin32.sourceforge.net
* bsdtar, flex, bison (and their dependencies)  
  http://gnuwin32.sourceforge.net/packages/libarchive.htm  
  http://gnuwin32.sourceforge.net/packages/bison.htm  
  http://gnuwin32.sourceforge.net/packages/flex.htm
* wget and OpenSSL (without dependencies)  
  http://gnuwin32.sourceforge.net/packages/wget.htm  
  http://gnuwin32.sourceforge.net/packages/openssl.htm

The locations of these tools must be added to the PATH environment variable.

Other prerequisites:

* Creating file and directory symbolic links must be authorized
* The environment must be set to the native Visual Studio tools

Pre-build commands:

```
mklink SConstruct tools\build\SConstruct
python third_party\fetch_packages.py

```

Build command:

```
scons
```
The above command is the equivalent of:

```
scons --jobs=1 --optimization=debug --root=C:\Contrail contrail-vrouter-agent
```
Another optimization option is "production", which produces an optimized build.
Two different optimization builds cannot be done in the same directory, unless
a clean is done between builds.

Clean command (scons -c does not work):
```
rmdir /q /s build
```

Install command:
```
scons install
```

* The above command needs additional arguments if the build was done with a
non default optimization or root

Test command:
```
scons test flaky-test
```

* Tests should not be run in parallel; building the tests in parallel is
possible, by defining the BUILD_ONLY environment variable prior to running the
scons command
* Tests need the build\lib and build\bin directories added to the PATH,
and assume as working directory the root of the sources' directory structure;
This must be done manually if tests are ran without using scons
* Tests, as well as contrail-vrouter-agent.exe, need updates to the firewall
configuration
