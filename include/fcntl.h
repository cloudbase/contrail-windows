#ifndef INCLUDE_FCNTL_H_
#define INCLUDE_FCNTL_H_

#include <next/fcntl.h>
#include <sys/stat.h>

#define O_RDONLY _O_RDONLY

#endif /* INCLUDE_FCNTL_H_ */
