#ifndef WINDOWS_SYS_STAT_H_
#define WINDOWS_SYS_STAT_H_

#include <next/sys/stat.h>

#define S_ISDIR(m) (((m) & S_IFMT) == S_IFDIR)

#endif /* WINDOWS_SYS_STAT_H_ */
