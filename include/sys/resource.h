#ifndef RESOURCE_H_
#define RESOURCE_H_

#include <stdint.h>

#define  RLIMIT_NOFILE 7

typedef uint32_t rlim_t;

struct rlimit {
    rlim_t rlim_cur;
    rlim_t rlim_max;
};

int getrlimit(int resource, struct rlimit *rlimits);

int setrlimit(int resource, const struct rlimit *rlimits);


#endif /* RESOURCE_H_ */
