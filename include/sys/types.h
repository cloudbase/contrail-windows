#ifndef INCLUDE_SYS_TYPES_H_
#define INCLUDE_SYS_TYPES_H_

#include <next/sys/types.h>
#include <stdint.h>

typedef uint8_t  u_int8_t;
typedef uint16_t u_int16_t;
typedef uint32_t u_int32_t;
typedef uint64_t u_int64_t;

typedef unsigned long int ulong;
typedef unsigned short int ushort;
typedef unsigned int uint;

typedef int pid_t;

#endif /* INCLUDE_SYS_TYPES_H_ */
