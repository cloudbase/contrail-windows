#ifndef TIME_H_
#define TIME_H_

#include <next/time.h>

#undef ctime_r

#ifdef __cplusplus
char *strptime(const char *buf, const char *fmt, struct tm *tm);
#endif

static __inline
time_t timegm(struct tm *tm) {
    return _mkgmtime(tm);
}

static __inline
struct tm *gmtime_r(const time_t *timep, struct tm *result) {
    const errno_t err = gmtime_s(result, timep);
    return err ? 0 : result;
}

static __inline
char *ctime_r(const time_t *timep, char *buf) {
    return ctime_s(buf, 26, timep) == 0 ? buf : 0;
}

#endif /* TIME_H_ */
