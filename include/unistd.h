#ifndef INCLUDE_UNISTD_H_
#define INCLUDE_UNISTD_H_

#include <windows.h>
#include <io.h>
#include <BaseTsd.h>
#include <process.h>

#undef usleep

#ifndef __ssize_t_defined
typedef SSIZE_T ssize_t;
# define __ssize_t_defined
#endif

static __inline
int isatty(int fd) {
    return _isatty(fd);
}

static __inline
int usleep(unsigned int usec) {
    // TODO: check if sub 1ms accuracy is necessary
    Sleep(0 < usec && usec < 1000 ? 1 : usec / 1000);
    return 0;
}

static __inline
unsigned int sleep(unsigned int sec) {
    Sleep(sec * 1000);
    return 0;
}

#endif /* INCLUDE_UNISTD_H_ */
