#ifndef INCLUDE_STRING_H_
#define INCLUDE_STRING_H_

#include <next/string.h>

static __inline
void bzero(void *s, size_t n) {
    (void) memset(s, 0, n);
}

static __inline
int strcasecmp(const char *s1, const char *s2) {
    return _stricmp(s1, s2);
}

static __inline
char *strtok_r(char *str, const char *delim, char **saveptr) {
    return strtok_s(str, delim, saveptr);
}

int ffs(int val);

#endif /* INCLUDE_STRING_H_ */
