#ifndef INCLUDE_NETINET_IN_H_
#define INCLUDE_NETINET_IN_H_

#include <stdint.h>
#include <Ws2tcpip.h>

typedef uint32_t in_addr_t;

#define IPPROTO_GRE     47

#endif /* INCLUDE_NETINET_IN_H_ */
