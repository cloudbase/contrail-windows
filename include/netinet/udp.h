#ifndef INCLUDE_NETINET_UDP_H_
#define INCLUDE_NETINET_UDP_H_

#include <stdint.h>

#ifndef ALT_IPHDR

struct udphdr {
    uint16_t uh_sport;
    uint16_t uh_dport;
    uint16_t uh_ulen;
    uint16_t uh_sum;
};

#else

struct udphdr
{
  uint16_t source;
  uint16_t dest;
  uint16_t len;
  uint16_t check;
};

#endif

#endif /* INCLUDE_NETINET_UDP_H_ */
