#ifndef INCLUDE_NETINET_IP_H_
#define INCLUDE_NETINET_IP_H_

#include <netinet/in.h>

struct ip {
    uint8_t ip_hl:4;  /* header length */
    uint8_t ip_v:4;   /* version */
    uint8_t ip_tos;        /* type of service */
    uint16_t ip_len;       /* total length */
    uint16_t ip_id;        /* identification */
    uint16_t ip_off;       /* fragment offset field */
#define IP_RF 0x8000       /* reserved fragment flag */
#define IP_DF 0x4000       /* dont fragment flag */
#define IP_MF 0x2000       /* more fragments flag */
#define IP_OFFMASK 0x1fff  /* mask for fragmenting bits */
    uint8_t ip_ttl;        /* time to live */
    uint8_t ip_p;          /* protocol */
    uint16_t ip_sum;       /* checksum */
    struct in_addr ip_src, ip_dst;  /* source and dest address */
};

struct iphdr
  {
    uint8_t ihl:4;
    uint8_t version:4;
    uint8_t tos;
    uint16_t tot_len;
    uint16_t id;
    uint16_t frag_off;
    uint8_t ttl;
    uint8_t protocol;
    uint16_t check;
    uint32_t saddr;
    uint32_t daddr;
    /*The options start here. */
  };

#endif /* INCLUDE_NETINET_IP_H_ */
