#ifndef WINDOWS_NETINET_IF_ETHER_H_
#define WINDOWS_NETINET_IF_ETHER_H_

#include <stdint.h>
#include <net/if_arp.h>
#include <net/ethernet.h>

struct  ether_arp {
    struct  arphdr ea_hdr;     /* fixed-size header */
    uint8_t arp_sha[ETH_ALEN]; /* sender hardware address */
    uint8_t arp_spa[4];        /* sender protocol address */
    uint8_t arp_tha[ETH_ALEN]; /* target hardware address */
    uint8_t arp_tpa[4];        /* target protocol address */
};
#define arp_hrd ea_hdr.ar_hrd
#define arp_pro ea_hdr.ar_pro
#define arp_hln ea_hdr.ar_hln
#define arp_pln ea_hdr.ar_pln
#define arp_op  ea_hdr.ar_op

#endif /* WINDOWS_NETINET_IF_ETHER_H_ */
