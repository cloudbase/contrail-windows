#ifndef INCLUDE_STDIO_H_
#define INCLUDE_STDIO_H_

#include <next/stdio.h>

#ifndef __ssize_t_defined
#include <BaseTsd.h>
typedef SSIZE_T ssize_t;
# define __ssize_t_defined
#endif

static __inline FILE *popen(const char *command, const char *mode) {
    return _popen(command, mode);
}

static __inline
int pclose(FILE *stream) {
    return _pclose(stream);
}

#if (_MSC_VER < 1900)
int snprintf(char *str, size_t size, const char *format, ...);
#endif

#endif /* INCLUDE_STDIO_H_ */
