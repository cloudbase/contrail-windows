#ifndef INCLUDE_WINDOWS_H_
#define INCLUDE_WINDOWS_H_

#ifndef NOMINMAX
#define NOMINMAX
#endif
#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif
#ifndef NOGDI
#define NOGDI
#endif
#include <next/windows.h>

#ifdef WIN_UNDEFS
#undef ERROR
#undef NO_ERROR
#undef DELETE
#undef EVENT_MAX
#endif

#endif /* INCLUDE_WINDOWS_H_ */
