#ifndef INCLUDE_NET_ETHERNET_H_
#define INCLUDE_NET_ETHERNET_H_

#include <net/if_ether.h>
#include <net/if.h>

#define ETH_ALEN    6
#define AF_BRIDGE   7

#define ETHER_ADDR_LEN  ETH_ALEN

#define ETHERTYPE_IP        0x0800
#define ETHERTYPE_ARP       0x0806
#define ETHERTYPE_VLAN      0x8100
#define ETHERTYPE_IPV6      0x86dd

struct ether_addr {
    uint8_t ether_addr_octet[ETH_ALEN];
};

#pragma pack(push, 1)
struct  ether_header {
        uint8_t  ether_dhost[ETH_ALEN];
        uint8_t  ether_shost[ETH_ALEN];
        uint16_t ether_type;
};
#pragma pack(pop)

#endif /* INCLUDE_NET_ETHERNET_H_ */
