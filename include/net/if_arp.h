#ifndef INCLUDE_NET_IF_ARP_H_
#define INCLUDE_NET_IF_ARP_H_

#define ARPOP_REQUEST   1       /* ARP request.  */
#define ARPOP_REPLY     2       /* ARP reply.  */

#define ARPHRD_ETHER    1       /* Ethernet 10/100Mbps.  */

struct arphdr
  {
    unsigned short int ar_hrd;      /* Format of hardware address.  */
    unsigned short int ar_pro;      /* Format of protocol address.  */
    unsigned char ar_hln;       /* Length of hardware address.  */
    unsigned char ar_pln;       /* Length of protocol address.  */
    unsigned short int ar_op;       /* ARP opcode (command).  */
  };

#endif /* INCLUDE_NET_IF_ARP_H_ */
